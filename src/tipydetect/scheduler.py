import time
import os
import subprocess
import threading
import datetime

import pip
from pkg_resources import get_distribution
import schedule

import tipydetect.config as Config
import crontab
from tipydetect.config import tipyLogger as log
from tipydetect.downloader import run as download, syncro_config, syncro_playlist, ftp_server
from tipydetect.control import MouseControl


def run():
    import datetime
    try:
        dates = ['dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi']
        now = datetime.datetime.now() + datetime.timedelta(minutes=1)
        next = now + datetime.timedelta(minutes=1)
        day = dates[now.isoweekday()]

        text="%s/%s:%s/hdmi_off\n%s/%s:%s/hdmi_on"%(day,now.hour,now.minute, day, next.hour, next.minute)
        sched = Scheduler()
        sched.schedule_rules(text)

        for job in schedule.jobs:
            log.debug(job)

        sched.start()
        sched.join()
    except(KeyboardInterrupt, SystemExit):
        sched.stop()

def touch(fname, mode=0o666, dir_fd=None, **kwargs):
    flags = os.O_CREAT | os.O_APPEND
    with os.fdopen(os.open(fname, flags=flags, mode=mode, dir_fd=dir_fd)) as f:
        os.utime(f.fileno() if os.utime in os.supports_fd else fname,
            dir_fd=None if os.supports_fd else dir_fd, **kwargs)

def get_uptime():
    with open('/proc/uptime','r') as f:
        uptime_seconds = float(f.readline().split()[0])
    return uptime_seconds

def send_boot_status():
    from tipydetect.analysis import statistics
    #print statistics
    statistics()

    #copy file for log upload processing
    log_file = os.path.join('/var/log/tipytv/', 'tipytv.log')
    import shutil
    shutil.copy2(log_file, log_file + '.boot')

    from tipydetect.uploader import run
    run()

def force_display():
    if os.path.exists('/tmp/video_off'):
        log.info('TipyDetect;MOUSE;ACTION;FORCE_DISPLAY;VIDEO_ON')
        video_on()
    if os.path.exists('/tmp/hdmi_off'):
        log.info('TipyDetect;MOUSE;ACTION;FORCE_DISPLAY;HDMI_ON')
        hdmi_on()

def video_off():
    log.info('Schedule;ACTION;VIDEO_OFF')
    touch('/tmp/video_off')

def video_on():
    log.info('Schedule;ACTION;VIDEO_ON')
    if os.path.exists('/tmp/video_off'):
        os.remove('/tmp/video_off')

def hdmi_off():
    log.info('Schedule;ACTION;HDMI_OFF')
    subprocess.call(['vcgencmd','display_power','0'])
    touch('/tmp/hdmi_off')

def hdmi_on():
    log.info('Schedule;ACTION;HDMI_ON')
    subprocess.call(['vcgencmd','display_power','1'])

    if os.path.exists('/tmp/hdmi_off'):
        os.remove('/tmp/hdmi_off')

def log_statistics():
    from tipydetect.analysis import statistics
    log.info('Schedule;ACTION;STATISTICS')
    statistics()

def cron_schedule(job, day, time):
    days = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
    if day != 'quotidien':
        job.dow.on(days.index(day) )

    hour,minute = time.split(':')
    job.hour.on(hour)
    job.minute.on(minute)

def reboot_now():
    os.system('/sbin/reboot')

def reboot( day, time):
    log.info('Schedule;ACTION;REBOOT CRONTAB')
    COMMAND = '/sbin/reboot'
    COMMENT = 'tipyTv reboot'

    # get all cron jobs for user
    user_cron = crontab.CronTab(user=True)
    # remove all related reboot jobs from crontab
    user_cron.remove_all(comment=COMMENT)
    #reschedule reboot in crontab
    reboot_job = user_cron.new(command=COMMAND, comment=COMMENT)

    try:
        #set date and time
        cron_schedule(reboot_job, day, time)
        user_cron.write()
    except:
        reboot_job.clear()
        user_cron.remove(reboot_job)

def print_debug():
    for job in schedule.jobs:
        log.debug('DEBUG;TIPYDETECT;%s;',str(job))

def run_threaded(job_func):
    job_thread = threading.Thread(target=job_func)
    job_thread.start()
    return job_thread


actions_dict = {
        'video_on' : video_on,
        'video_off' : video_off,
        'hdmi_on' : hdmi_on,
        'hdmi_off' : hdmi_off,
        'reboot': reboot,
        'statistics': log_statistics,
        }


class Scheduler(threading.Thread):

    def __init__(self):
        super(Scheduler, self).__init__()

    def retrieve_config(self):
        log.info('Scheduler;MEDIAPLAYLIST;GET_CONFIG;START')
        config = Config.config()

        #reparse HDMI rules
        try:
            configfile = os.path.join(
                config['medias']['path'],'cfg',
                '%s.rules'%config['account']['user_id']
                )
            #cancel all old rules
            schedule.clear('tipytv')
            self.schedule_tipytv_rules(configfile)

        except Exception as e:
            log.warning('Scheduler;CONFIG;NOMEDIAFILES;%s',e)
            raise(e)

        #reparse update rules
        try:
            configfile =  os.path.join(
                    config['medias']['path'], 'cfg',
                    '%s.cfg'%config['account']['user_id'])

            #cancel all old rules
            schedule.clear('config')
            schedule.clear('playlist')

            self.schedule_updates(configfile)
        except Exception as e:
            log.warning('Scheduler;CONFIG;NOMEDIAFILES;%s',e)
            raise(e)

        log.info('Scheduler;MEDIAPLAYLIST;GET_CONFIG;STOP')
        return True

    def schedule_updates(self, configfile):
        playlist_job = False
        config_job = False
        update_job = False
        must_reboot = False
        packages = dict()

        with open(configfile, 'r') as f:
            for line in f:
                line = line.strip()
                try:
                    key, phrase = line.split('=')
                    if key.upper() == 'DELAI_GET_PLAYLIST_VIDEO':
                        log.info('Scheduler;SCHEDULE;GETPLAYLISTVIDEO;%s',phrase)
                        schedule.every(int(phrase)).minutes.do(syncro_playlist).tag('playlist')
                        playlist_job = True
                    elif key.upper() == 'DELAI_GET_CONFIG':
                        log.info('Scheduler;SCHEDULE;GETCONFIG;%s',phrase)
                        schedule.every(int(phrase)).minutes.do(run_threaded, syncro_config).tag('config')
                        config_job = True
                    elif key.upper() == 'PACKAGES':
                        log.info('Scheduler;SCHEDULE;GETPACKAGES;%s',phrase)
                        for package in phrase.split(';'):
                            packages[package] = dict()
                        update_job = True
                    elif '_VERSION' in key.upper():
                        log.info('Scheduler;SCHEDULE;PACKAGE_VERSION;%s %s',key, phrase)
                        package = key[:-8]
                        if package in packages.keys():
                            packages[package]['version'] = phrase
                    elif '_URL' in key.upper():
                        log.info('Scheduler;SCHEDULE;PACKAGE_URL;%s %s',key, phrase)
                        package = key[:-4]
                        if package in packages.keys():
                            packages[package]['url'] = phrase

                except Exception as e:
                    log.debug('Scheduler;SCHEDULE;PARSING_ERROR;%s;%s',line,e)

        if update_job:
            log.warning('Scheduler;SCHEDULE;START UPDATE')

            for name,package in packages.items():
                try:
                    try:
                        package_version = get_distribution(name).version
                    except pkg_resources.DistributionNotFound as e:
                        package_version = None

                    if package['version'] != package_version:
                        res = pip.main(['install', '--upgrade', package['url']])
                        must_reboot = True

                        log.info('UPDATE;UPDATE;%s : %s -> %s',
                                name,
                                package_version,
                                package['version'])
                    else:
                        log.info('UPDATE;UP_TO_DATE;%s : %s',
                                name,
                                package_version)

                except Exception as e:
                    log.warning('UPDATE;FAIL;%s;%s',package,e)

            #at least one apckage has been updated, need reboot
            if must_reboot:
                reboot_now()

        if not playlist_job:
            schedule.every(10).minutes.do(syncro_playlist).tag('playlist')
            log.warning('Scheduler;SCHEDULE;GETPLAYLISTVIDEO;DEFAULT;every 10 min')

        if not config_job:
            schedule.every().hour.do(run_threaded, syncro_config).tag('config')
            log.warning('Scheduler;SCHEDULE;GETCONFIG;DEFAULT;every  hour')

    def schedule_tipytv_rules(self, configfile):
        with open(configfile, 'r') as f:
            txt = f.read()

        rules = [ extract.split('/') for extract in txt.splitlines()]

        for day,time,action_name in rules:
            #Fix: reboot is handled by Crontab, not schedule
            if action_name == 'reboot':
                reboot(day, time)
                continue

            else:
                try:
                    action = actions_dict.get(action_name, lambda:None)
                except (KeyError):
                    log.warning("Rule %s at %s %s %s does not exist", action, day, time, action)
                    continue

                if day == 'lundi':
                    schedule.every().monday.at(time).do(run_threaded, action).tag('tipytv')
                elif day == 'mardi':
                    schedule.every().tuesday.at(time).do(run_threaded, action).tag('tipytv')
                elif day == 'mercredi':
                    schedule.every().wednesday.at(time).do(run_threaded, action).tag('tipytv')
                elif day ==  'jeudi':
                    schedule.every().thursday.at(time).do(run_threaded, action).tag('tipytv')
                elif day == 'vendredi':
                    schedule.every().friday.at(time).do(run_threaded, action).tag('tipytv')
                elif day == 'samedi':
                    schedule.every().saturday.at(time).do(run_threaded, action).tag('tipytv')
                elif day == 'dimanche':
                    schedule.every().sunday.at(time).do(run_threaded, action).tag('tipytv')
                elif day == 'quotidien':
                    schedule.every().day.at(time).do(run_threaded, action).tag('tipytv')

            log.info('Scheduler;SCHEDULE;%s;%s;%s',day, time, action_name)


    def run(self):
        controler = MouseControl(
                interrupt_callback = force_display)
        controler.start()

        log.info('Scheduler;SCHEDULE;START')

        config = Config.config()
        user_id = config['account']['user_id']

        send_boot_status()
        reset = True

        timestamp = datetime.datetime.now()
        elapsed_time = get_uptime()


        while True:
            #Test if delta is not too much important
            now = datetime.datetime.now()
            delta = get_uptime() - elapsed_time

            if abs(now - timestamp) > datetime.timedelta(seconds=delta+600):
                log.warning('Scheduler;SCHEDULE;TIMEGAP')
                timestamp = now
                elapsed_time = get_uptime()
                reset = True

            # test if user_id has changed and reset configuration if yes
            config = Config.config()
            if user_id != config['account']['user_id']:
                log.warning('Scheduler;SCHEDULE;NEW_USERID')
                user_id = config['account']['user_id']
                reset = True

            if reset:
                # remove all scheduled tasks
                # will be reschedule if needed
                log.warning('Scheduler;SCHEDULE;RESET_CONFIG')
                schedule.clear()

                #remove previous download_queue
                ftp_server.clear_queue()

                log.warning('Scheduler;SCHEDULE;CLEAR TASKS')
                schedule.every(10).minutes.do(print_debug).tag('debug')

                try:
                    #get files for configuration: BLOCKING
                    syncro_config()
                    ftp_server.download_queue.join()

                    #then parse config files
                    self.retrieve_config()

                    #get media files: NON BLOCKING
                    syncro_playlist()
                    reset = False
                except:
                    reset = True


            schedule.run_pending()
            time.sleep(5)

        controler.stop()
        log.info('Scheduler;SCHEDULE;STOP')


if __name__ == "__main__":
    run()
