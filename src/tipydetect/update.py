from pkg_resources import get_distribution
import pip

import tipydetect.config as Config
from tipydetect.config import tipyLogger as log


def tipy_update():
    global ftp_server
    log.info('UPDATE;START')
    must_reboot = False

    #read config from file or get default parameters
    config = Config.config()

    if not config.has_section('update'):
        exit(0)

    for package in config['update']['packages'].split():
        try:
            package_version = get_distribution(package).version
            if config['update'][package + '_version'] != package_version:
                if pip.main(['install', '--force-reinstall', config['update'][package + '_url']]):
                    log.info('UPDATE;FAILED;%s : %s -> %s',
                        package,
                        package_version,
                        get_distribution(package).version )
                else:
                    log.info('UPDATE;UPDATE;%s : %s -> %s',
                            package,
                            package_version,
                            get_distribution(package).version )
                    must_reboot = True
            else:
                log.info('UPDATE;UPTODATE;%s : %s',
                        package,
                        package_version)

        except:
            log.warning('UPDATE;FAIL;%s',package)

    log.info('UPDATE;STOP')
    return must_reboot

if __name__ == '__main__':
    run()
