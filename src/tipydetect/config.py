import logging
import logging.handlers

import os

#get configuration from config file if exists
import configparser
from pkg_resources import resource_string

tipyLogger = logging.getLogger(__name__)
tipyLogger.setLevel(logging.DEBUG)

ch=logging.FileHandler('/var/log/tipytv/tipytv.log')

# create formatter
formatter = logging.Formatter('%(asctime)s;%(levelname)s;%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
ch.setFormatter(formatter)

tipyLogger.addHandler(ch)
tipyLogger.addHandler(logging.StreamHandler())

def config():

    tipyConfig = configparser.SafeConfigParser()

    #read first init file
    ini_conf = resource_string(__name__, 'data/tipy.ini')
    tipyConfig.read_string(ini_conf.decode('utf8'))

    #read runtime based conf file
    conf_file = os.path.expanduser(tipyConfig['tipy']['conf'])
    if os.path.exists(conf_file):
        tipyConfig.read(conf_file)

    return tipyConfig
