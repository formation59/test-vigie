import os
import threading
import socket

from queue import Queue
from time import sleep

from datetime import datetime

import traceback
import ftplib
import socket

#md5sum computing
import hashlib

import xml.etree.ElementTree as ET
from tinydb import TinyDB, Query

import tipydetect.config as Config
from tipydetect.config import tipyLogger as log



def relative_path(src):
    config = Config.config()
    return [src, os.path.join(config['medias']['path'], src)]

def run(full=True):
    global ftp_server

    #read config from file or get default parameters
    config = Config.config()

    ftp_server = FTPServer(
            config['ftp']['server'],
            config['ftp']['username'],
            config['ftp']['password']
            )
    ftp_server.daemon = True
    ftp_server.start()

    dl = Downloader( full=full)
    return dl.run()


class FTPServer(threading.Thread):

    def __init__(self):
        super(FTPServer, self).__init__()
        self.connection = None
        self.download_queue = Queue()

    def download(self, origin, dest, callback=None, callback_args=None):
        #test if not already present in queue
        #if present, do nothing: avoid multiple downloads
        if origin in [item[0] for item in self.download_queue.queue]:
            log.info('FTPServer;DOWNLOAD_QUEUE;ALREADY WAITING;%s;%s', origin,dest)
        else:
            log.info('FTPServer;DOWNLOAD_QUEUE;ADD;%s;%s', origin,dest)
            self.download_queue.put([origin, dest, callback, callback_args])

    def clear_queue(self):
        while not self.download_queue.empty():
            try:
                self.download_queue.get(False)
            except Empty:
                continue
            self.download_queue.task_done()


    def run(self):
        log.info('FTPServer;START;')
        self.reset_connection()
        retry = 0

        while True:
            if retry == 0 and self.download_queue.empty():
                sleep(1)
                continue

            #check retry counter, if > 0 keep old item
            if retry == 0:
                src, dest, callback, callback_args = self.download_queue.get()

            try:
                log.info('FTPServer;FTP;RETR;%s;%s'%(src,dest))

                #automatically create dir (recursively if needed)
                #to record destination file
                _dir,_ = os.path.split(dest)
                if not os.path.isdir(_dir):
                    os.makedirs(_dir)

                self.connection.retrbinary('RETR %s'%src, open(dest, 'wb').write)
                log.info('FTPServer;FTP;RETR;OK')

                # success: reinitialize counter to 0
                retry = 0

            except ftplib.all_errors as e:
                #In case of failure reset FTP connection
                #but keep download queue
                log.warning('FTPServer;FTP;ERROR;%s',str(e))
                self.reset_connection()

                # increment retry counter
                # cancel after 3 retries
                if retry == 2:
                    self.download_queue.task_done()
                    retry = 0
                else:
                    retry = (retry+1)%3
                continue

            except Exception as e:
                log.warning('FTPServer;DOWNLOAD;ERROR;%s',str(e))
                self.download_queue.task_done()
                retry = 0
                continue

            if callback:
                try:
                    callback(*callback_args) if callback_args else callback()
                except Exception as e:
                    log.warning('FTPServer;FTP;CALLBACK FAIL ERROR;%s',e)

            #call download_queue.task_done at end
            #to ensure that files have been downloaded
            #otherwise self.download_queue.join() is useless
            self.download_queue.task_done()

        return True


    def reset_connection(self):
        log.info('FTPServer;RESET')
        config = Config.config()

        if self.connection:
            log.info('FTPServer;CLOSE;PREVIOUS CONNECTION FAILED')
            self.connection.close()

        self.connection = ftplib.FTP(
            config['ftp']['server'],
            user = config['ftp']['username'],
            passwd = config['ftp']['password'],
            timeout = 300
            )


ftp_server = FTPServer()
ftp_server.start()

def syncro_config():
    global ftp_server
    user_id = Config.config()['account']['user_id']

    log.debug('Downloader;CONFIG;GET;%s',user_id)
    path = os.path.join( 'cfg','%s.cfg'%user_id)
    src, cfg_file = relative_path(path)
    ftp_server.download(src, cfg_file)

    path = os.path.join( 'cfg','%s.rules'%user_id)
    src, rules_file = relative_path(path)
    ftp_server.download(src, rules_file)

    return True

def syncro_playlist(joining = False):
    job_thread = PlaylistSyncroThread()
    job_thread.start()
    if joining:
        job_thread.join()

class PlaylistSyncroThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(PlaylistSyncroThread, self).__init__( *args, **kwargs)
        self.config = Config.config()
        self.user_id = self.config['account']['user_id']

        try:
            self.db = TinyDB(os.path.join(self.config['medias']['path'], self.config['medias']['db']))
        except Exception:
            log.warning('Downloader;DB;INIT;PROBLEM; RM DB')
            os.remove(os.path.join(self.config['medias']['path'], self.config['medias']['db']))


    def run(self):
        global ftp_server
        log.debug('Downloader;CATALOG;GET;%s',self.user_id)

        path = os.path.join( 'playlists','video','%s.xml'%self.user_id)
        src, media_file = relative_path(path)

        ftp_server.download(src, media_file,
                callback = self.parse_catalog_medias,
                callback_args=(media_file, 'media'))

        path = os.path.join('playlists','pubs','video','%s.xml'%self.user_id)
        src, pubs_file = relative_path(path)

        ftp_server.download(src, pubs_file,
                callback = self.parse_catalog_medias,
                callback_args=(pubs_file, 'pub'))

    def parse_catalog_medias(self, src, media_type):
        log.info('Downloader;CATALOG;PARSE')
        root = ET.parse(src).getroot()

        log.info('Downloader;CATALOG;CHECK')
        for media in root.iter('information'):
            id = media.find('id').text
            last_modification = media.find('date_modification').text
            score = media.find('score').text

            if not score:
                log.warning('Downloader;CATALOG;IGNORE;%s;%s', id, media_type)
                continue


            media_description= {
                        'type': media_type,
                        'id':id,
                        'last_modification':last_modification,
                        'score':score,
                        }
            self.check_media(media_description)

        # cleaning phase
        catalog = self.db.table('catalog')
        query = Query()
        now = datetime.strftime(datetime.now(), '%Y/%m/%d')
        xml_ids = [ elt.text for elt in root.findall('./information/id')]

        #remove files and db entry for outdated medias
        log.info('Downloader;CATALOG;CLEAN')
        for media in catalog.search(query.type == media_type):
            # media has been discard from xml, remove from playlist
            if not media['id'] in xml_ids:
                log.debug('Downloader;CATALOG;MEDIA;NOT_IN_XML;%s;%s', media['id'], media['video'])
                if media['end_date'] != '':
                    try:
                        log.debug('Downloader;CATALOG;MEDIA;CLEAN;DISK RM %s', media['video'])
                        os.remove(os.path.join(self.config['medias']['path'],'uploads',media['video']))
                    except FileNotFoundError as m:
                        log.debug('Downloader;CATALOG;MEDIA;FileNotFound RM %s failed!', media['video'])

                log.debug('Downloader;CATALOG;MEDIA;CLEAN;DB RM %s', media['id'])
                catalog.remove(query.id == media['id'])

            # media is outdated, remove from playlist
            elif media['end_date'] != '' and media['end_date'] < now:
                log.debug('Downloader;CATALOG;MEDIA;OUTDATED;%s;%s', media['id'], media['video'])
                try:
                    log.debug('Downloader;CATALOG;MEDIA;CLEAN;DISK RM %s', media['video'])
                    os.remove(os.path.join(self.config['medias']['path'],'uploads',media['video']))
                except FileNotFoundError as m:
                    log.debug('Downloader;CATALOG;MEDIA;RM:%s', m)

                log.debug('Downloader;CATALOG;MEDIA;CLEAN;DB RM %s', media['id'])
                catalog.remove(query.id == media['id'])


    def check_media(self, desc):
        log.debug('Downloader;CATALOG;MEDIA;CHECK;%s;%s', desc['id'], desc['type'])
        catalog = self.db.table('catalog')
        query = Query()

        if catalog.contains(query.id == desc['id']):
            log.debug('Downloader;CATALOG;MEDIA;ALREADY_IN_DB;%s', desc['id'])
            media = catalog.get(query.id == desc['id'])
            #force DB removal and download if modification date has changed
            if (media['last_modification'] != desc['last_modification'] ):
                log.debug('Downloader;CATALOG;MEDIA;NEW_MODIFICATION_DATE;;%s', desc['id'])
                catalog.remove(query.id == media['id'])
                self.get_media_description(desc)
            if not os.path.exists(os.path.join(self.config['medias']['path'],'uploads',media['video'])):
                log.debug('Downloader;CATALOG;MEDIA;NO_FILE;%s;%s', desc['id'], media['video'])
                catalog.remove(query.id == media['id'])
                self.get_media_description(desc)
        else:
            log.debug('Downloader;CATALOG;MEDIA;NEW ITEM;%s', desc['id'])
            self.get_media_description(desc)


    def get_media_description(self, desc):
        log.debug('Downloader;CATALOG;MEDIA;GET;%s;%s', desc['type'],desc['id'])

        query = Query()
        src, dest = relative_path(os.path.join('datas','%s.data'%desc['id']))

        ftp_server.download(src, dest,
            callback = self.insert_media,
            callback_args = (desc, dest))

    def insert_media(self, desc, src):
        log.debug('Downloader;CATALOG;MEDIA;INSERT;%s',desc['id'])
        root = ET.parse(src)

        video = root.find('video').text
        if not video:
            log.warning('Downloader;CATALOG;MEDIA;INSERT_FAILED;%s no video tag',media_id)
            return

        start_date = root.find('date_debut').text
        end_date = root.find('date_fin').text or ''
        last_modification = root.find('date_modification').text
        name = root.find('nom').text or ''
        keywords = root.find('mots_clefs').text or ''
        md5sum = root.find('md5').text or ''

        #test end_date before inserting media
        now = datetime.strftime(datetime.now(), '%Y/%m/%d')
        if end_date != '' and end_date < now :
            log.warning('Downloader;CATALOG;MEDIA;INSERT_CANCELED;%s end of diffusion %s',
                    desc['id'],
                    end_date)
            return

        parameters = {'start_date':start_date, 'end_date':end_date,
                'last_modification':last_modification, 'video':video,
                'name':name, 'keywords':keywords, 'md5sum':md5sum}
        parameters.update(desc)

        src, dest = relative_path(os.path.join('uploads', video))
        if not os.path.exists(dest):
            ftp_server.download(src, dest,
                    callback = self.catalog_insert,
                    callback_args = (parameters,dest))
        else:
            self.catalog_insert(parameters, dest)

    def catalog_insert(self, parameters, fname):
        catalog = self.db.table('catalog')
        # check md5sum of downloaded file
        md5 = hashlib.md5()
        with open(fname, "rb") as f:
             for chunk in iter(lambda: f.read(4096), b""):
                 md5.update(chunk)

        if (md5.hexdigest() == parameters['md5sum']):
            catalog.insert(parameters)
        else:
            log.warning('Downloader;CATALOG;MEDIA;INSERT_FAILED;check md5sum %s failed',parameters['video'])
            os.remove(fname)


if __name__ == '__main__':
    run()
