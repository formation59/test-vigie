import os
import threading
from queue import Queue
from time import sleep

from datetime import datetime

import traceback
import ftplib

import tipydetect.config as Config
from tipydetect.scheduler import touch
from tipydetect.config import tipyLogger as log


ftp_server = None

def run(full=True):
    global ftp_server

    #read config from file or get default parameters
    config = Config.config()

    ftp_server = FTPServer(
            config['ftp']['server'],
            config['ftp']['username'],
            config['ftp']['password']
            )
    ftp_server.daemon = True
    ftp_server.start()

    up = LogUploader()
    up.start()
    up.join()


class FTPServer(threading.Thread):

    def __init__(self, url, user, passwd):
        super(FTPServer, self).__init__()
        self.connection = None
        self.url = url
        self.user = user
        self.passwd = passwd
        self.upload_queue = Queue()


    def upload(self, origin, dest, callback=None, callback_args=None):
        self.upload_queue.put([origin, dest, callback, callback_args])

    def run(self):
        log.info('LogFTPServer;START;%s;%s',self.url,self.user)
        while True:
            if self.upload_queue.empty():
                sleep(1)
                continue

            item = self.upload_queue.get()
            src, dest, callback, callback_args = item

            try:
                if not self.connection:
                    self.reset_connection()

                log.debug('LogFTPServer;UPLOAD;%s;%s'%(src,dest))

                self.connection.storbinary('STOR ' + dest, open(src, 'rb'))
                log.debug('LogFTPServer;UPLOADED')

                # update connection status if FTP upload is OK
                touch('/tmp/ftp_upload')

                if callback:
                    try:
                        callback(*callback_args if callback_args else None)
                    except Exception as m:
                        traceback.print_exc()
                        continue

            except ftplib.all_errors as e:
                log.debug(e)
                self.reset_connection()
                self.upload_queue.put(item)

                # update connection status if FTP upload is OK
                if os.path.exists('/tmp/ftp_upload'):
                    os.remove('/tmp/ftp_upload')

            except Exception as e:
                log.debug(e)
                callback = None

                # update connection status if FTP upload is OK
                if os.path.exists('/tmp/ftp_upload'):
                    os.remove('/tmp/ftp_upload')
            finally:
                self.upload_queue.task_done()
                log.debug('LogFTPServer;STOP;%s'%self.url)

    def reset_connection(self):
        log.info('LogFTPServer;RESET')
        if self.connection:
            try:
                log.debug('LogFTPServer;CLOSE;%s'%self.url)
                self.connection.close()
            finally:
                self.connection = None

        self.connection = ftplib.FTP(self.url, user=self.user, passwd=self.passwd)
        log.info('LogFTPServer;RESTART;%s',self.url)


class LogUploader(threading.Thread):
    def __init__(self, full=False):
        super(LogUploader, self).__init__()


    def run(self):
        global ftp_server
        log.info('LogUploader;START')
        config = Config.config()

        base_dir = '/var/log/tipytv/'

        for dir,_,files in os.walk(base_dir):
            for f in files:
                if f.startswith("tipytv.log."):
                    try:
                        with open(os.path.join(dir,f),'r') as log_file:
                            first_line = log_file.readline()
                        date = datetime.strptime(first_line.split(';')[0], '%Y-%m-%d %H:%M:%S')
                    except:
                        log.error('LogUploader;BAD_TIMESTAMP;%s',f)
                        continue

                    src = os.path.join(dir,f)
                    #rewrite rule for dest file
                    dest = '/log/log_%s_%s.txt'%(config['account']['user_id'],
                            date.strftime('%Y%m%d%H%M'))

                    ftp_server.upload(src, dest, callback=os.remove, callback_args=[src,])

        #wait that all catalogs have been parsed
        ftp_server.upload_queue.join()
        log.info('LogUploader;STOP')


if __name__ == '__main__':
    run()
