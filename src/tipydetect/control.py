import sys
import time
import threading

import gi
gi.require_version("Gtk", "3.0")

from gi.repository import GLib

from tipydetect.config import tipyLogger as log

def run ():
    try:
        p = MouseControl()
        p.start()
        #wait for thread to finish
        p.join()
    except (KeyboardInterrupt, SystemExit):
        p.stop()
        sys.exit()


class MouseControl(threading.Thread):
    delay = 30

    def __init__(self, interrupt_callback = lambda:None, idle_callback=lambda:None):
        super(MouseControl, self).__init__()
        self.input = open('/dev/input/mice','rb')

        self.notify_event = threading.Event()

        self.interrupt_callback = interrupt_callback
        self.idle_callback = idle_callback

        self.idle_timer = threading.Timer(self.delay,self.idle_callback)


    def interrupt(self):
        try:
            self.interrupt_callback()
        finally:
            self.stop()

    def idle(self):
        try:
            self.idle_callback()
        finally:
            self.restart()


    def run(self):
        log.debug('IdleControl;START')
        #initialize Event before main loop
        self.notify_event.set()

        while self.input.read(10):
            log.debug('IdleControl;MOUSE;DETECT')
            try:
                if self.notify_event.is_set():
                    self.interrupt()

                #reset the timer
                self.idle_timer.cancel()
                self.idle_timer = threading.Timer(self.delay, self.idle)
                self.idle_timer.start()
            except Exception as m:
                log.warning(m)

            time.sleep(1)
        log.debug('IdleControl;STOP')

    def restart(self):
        log.debug('IdleControl;RESTART')
        self.notify_event.set()

    def stop(self):
        log.debug('IdleControl;PAUSE')
        self.notify_event.clear()

if __name__ == '__main__':
    run()
