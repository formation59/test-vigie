import psutil
import socket
import sys
import os
from tipydetect.config import tipyLogger as log, config


def tipy():
    conf = config()
    for section in conf.sections():
        log.info( '[ %s ]', section)
        for option in conf.options(section):
            log.info('    %s = %s', option, conf[section][option])

def statistics():
    tipy()
    memory()
    disk()
    network()

def memory():
    virt = psutil.virtual_memory()
    swap = psutil.swap_memory()
    templ = "%-7s %10s %10s %10s %10s %10s %10s"
    log.info(templ % ('', 'total', 'used', 'free', 'shared', 'buffers', 'cache'))
    log.info(templ % (
        'Mem:',
        int(virt.total / 1024),
        int(virt.used / 1024),
        int(virt.free / 1024),
        int(getattr(virt, 'shared', 0) / 1024),
        int(getattr(virt, 'buffers', 0) / 1024),
        int(getattr(virt, 'cached', 0) / 1024)))
    log.info(templ % (
        'Swap:', int(swap.total / 1024),
        int(swap.used / 1024),
        int(swap.free / 1024),
        '',
        '',
        ''))
    log.info("")

def bytes2human(n):
    # http://code.activestate.com/recipes/578019
    # >>> bytes2human(10000)
    # '9.8K'
    # >>> bytes2human(100001221)
    # '95.4M'
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n


def disk():
    templ = "%-17s %8s %8s %8s %5s%% %9s  %s"
    log.info(templ % ("Device", "Total", "Used", "Free", "Use ", "Type",
                   "Mount"))
    for part in psutil.disk_partitions(all=False):
        if os.name == 'nt':
            if 'cdrom' in part.opts or part.fstype == '':
                # skip cd-rom drives with no disk in it; they may raise
                # ENOENT, pop-up a Windows GUI error for a non-ready
                # partition or just hang.
                continue
        usage = psutil.disk_usage(part.mountpoint)
        log.info(templ % (
            part.device,
            bytes2human(usage.total),
            bytes2human(usage.used),
            bytes2human(usage.free),
            int(usage.percent),
            part.fstype,
            part.mountpoint))
    log.info("")

af_map = {
    socket.AF_INET: 'IPv4',
    socket.AF_INET6: 'IPv6',
    psutil.AF_LINK: 'MAC',
}

duplex_map = {
    psutil.NIC_DUPLEX_FULL: "full",
    psutil.NIC_DUPLEX_HALF: "half",
    psutil.NIC_DUPLEX_UNKNOWN: "?",
}


def network():
    stats = psutil.net_if_stats()
    io_counters = psutil.net_io_counters(pernic=True)
    for nic, addrs in psutil.net_if_addrs().items():
        log.info("%s:" % (nic))
        if nic in stats:
            st = stats[nic]
            log.info("    stats          : speed=%sMB, duplex=%s, mtu=%s, up=%s" % (
                st.speed, duplex_map[st.duplex], st.mtu,
                "yes" if st.isup else "no"))
        if nic in io_counters:
            io = io_counters[nic]
            log.info("    incoming       : bytes=%s, pkts=%s, errs=%s, drops=%s" % (
                io.bytes_recv, io.packets_recv, io.errin, io.dropin))
            log.info("    outgoing       : bytes=%s, pkts=%s, errs=%s, drops=%s" % (
                io.bytes_sent, io.packets_sent, io.errout, io.dropout))
        for addr in addrs:
            log.info("    %-4s" % af_map.get(addr.family, addr.family))
            log.info("         address   : %s" % addr.address)
            if addr.broadcast:
                log.info("         broadcast : %s" % addr.broadcast)
            if addr.netmask:
                log.info("         netmask   : %s" % addr.netmask)
            if addr.ptp:
                log.info("      p2p       : %s" % addr.ptp)
        log.info("")

