#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import sys
import os
import threading
import configparser

import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk
from gi.repository import Gdk

from tipyplayer.player import Player
import tipyplayer.control as control

import tipydetect.config as Config
from tipydetect.config import tipyLogger as log

def run():
    try:
        win = MainWindow()
        win.connect('delete-event', Gtk.main_quit)
        win.show_all()

        Gtk.main()

    except (KeyboardInterrupt, SystemExit):
        #stop all threads
        stop(win)

    #exit sytem
    sys.exit()


def stop(win):
    if not win.emit("delete-event", Gdk.Event(gtk.gdk.DELETE)):
        win.destroy()



class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="tipyTV")

        #config initialization
        config = Config.config()
        self.config_file = os.path.expanduser(config['tipy']['conf'])

        self.config = configparser.SafeConfigParser()
        self.config.read(self.config_file)

        #prepare screen for configuration parameters
        self.options_widget = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        #user name
        row = Gtk.Box(spacing=6)
        row.pack_start(Gtk.Label("Serveur FTP"), True, True, 0)
        self.server = Gtk.Entry()

        if self.config.has_option('ftp','server'):
            self.server.set_text(self.config['ftp']['server'])
        row.pack_start(self.server, True, True, 0)

        self.options_widget.pack_start(row, True, True, 0)
        #user name
        row = Gtk.Box(spacing=6)
        row.pack_start(Gtk.Label("Identifiant FTP"), True, True, 0)
        self.username = Gtk.Entry()

        if self.config.has_option('ftp','username'):
            self.username.set_text(self.config['ftp']['username'])
        row.pack_start(self.username, True, True, 0)

        self.options_widget.pack_start(row, True, True, 0)

        #password
        row = Gtk.Box(spacing=6)
        row.pack_start(Gtk.Label("Mot de passe"), True, True, 0)
        self.password = Gtk.Entry()

        if self.config.has_option('ftp','password'):
            self.password.set_text(self.config['ftp']['password'])
        row.pack_start(self.password, True, True, 0)

        self.options_widget.pack_start(row, True, True, 0)

        #save button
        save_btn = Gtk.Button.new_with_label("Sauver")
        save_btn.connect("clicked", self.save_config)
        self.options_widget.pack_start(save_btn, True, True, 0)

        self.add(self.options_widget)


    def save_config(self, *args):
        config = Config.config()
        try:
            username = self.username.get_text()
            password = self.password.get_text()
            server = self.server.get_text()

            if not self.config.has_section('ftp'):
                self.config.add_section('ftp')

            if len(username) == 0 and self.config.has_option('ftp','username'):
                self.config.remove_option('ftp','username')
            else:
                self.config.set('ftp','username', username)

            if len(password) == 0 and self.config.has_option('ftp','password'):
                self.config.remove_option('ftp','password')
            else:
                self.config.set('ftp','password', password)

            if len(server) == 0 and self.config.has_option('ftp','server'):
                self.config.remove_option('ftp','server')
            else:
                self.config.set('ftp','server', server)

            with open(self.config_file,'w') as config_file:
                self.config.write(config_file)

        except Exception as m:
            log.warning('Unable to save config paramaters', m)


if __name__ == "__main__":
    run()
