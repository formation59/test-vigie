#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import subprocess

from tipydetect.config import tipyLogger as log

def run():
    from tipydetect.scheduler import Scheduler
    scheduler = Scheduler()
    scheduler.start()

def check_bootstrap():
    bootstrap_file = '/home/pi/tipydetect.bootstrap'
    if os.path.exists(bootstrap_file):
        log.warning('Detect;BOOTSTRAP;execute %s'%bootstrap_file)
        subprocess.call(bootstrap_file)
    else:
        log.warning('Detect;BOOTSTRAP;%s not exist'%bootstrap_file)

if __name__ == '__main__':
    check_bootstrap()
    run()
