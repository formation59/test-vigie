#!/usr/bin/env python3

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(name='tipyDetect',
        version='0.6.7',
        description='Configuration  module for OTV VIGIE Raspian based solution',
        author='Romain TALDU',
        author_email='romain.taldu@gmail.com',

        packages=['tipydetect',],
        package_dir={'tipydetect':'src/tipydetect',},
        package_data={'tipydetect':['data/tipy.ini',]},

        scripts = ['scripts/launch_tipydetect.py',
            'scripts/launch_tipylogsupload.py',
            'scripts/launch_tipyupdate.py',
            'scripts/launch_tipyconfig.py'],

        data_files = [
            ('/home/pi/.config/systemd/user', ['systemd/tipydetect.service']),
            ('/home/pi/', ['systemd/tipydetect.bootstrap']),
            ],
        )


